# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


import json
from spiders.exceptions import JSONReadError
from scrapy.exceptions import DropItem
import datetime
import hashlib

class CheckDataValidityPipeline(object):

	def open_spider(self, spider):
		pass

	def close_spider(self, spider):
		pass

	def process_item(self, entry, spider):
		""" 
		Checks item before saving it.
		
		:param spider:
			Spider crawler instance, class.
		
		:param entry:
			Data to save, dict.
		
		
		"""
		key_types = {
			'int': int,
			'list': list,
			'str': unicode,
			'date': datetime.datetime
		}
		for key in spider.config['scrape']['required_keys']:
			try:
				entry[key]
			except KeyError:
				raise DropItem(" Object in pipeline missing required key: %s, dropping item" % entry)
			
			if not isinstance(entry[key], key_types[spider.config['scrape']['object_mapping'][key]['type']]):
				raise DropItem("Dropping item: %s because key %s not right type: %s, expected: %s" % (entry,key, type(entry[key]), spider.config['scrape']['object_mapping'][key]['type']) )
		return entry
			

class JsonWriterPipeline(object):

	def open_spider(self, spider):
		self.file_name = 'full_output_scrapy.json'

	def close_spider(self, spider):
		pass

	def _is_duplicate(self, md5_hash, hashes):
		""" 
		Checks if hash of item is has already been seen.
		"""

		if md5_hash in hashes:
			return True
		return False

	def process_item(self, entry, spider, over_write = False):
		""" 
		Writes json file out.
		
		:param file_name:
			What is the file name to read/create, string.
		
		:param entry:
			Data to save, dict.
		
		:param over_write:
			Whether to add to the file or create it fresh, defaults to False, boolean.
		
		"""
		file_name = self.file_name
		entry = dict(entry)
		md5_hash = hashlib.md5(str(entry)).hexdigest()
		entry['md5_hash'] = md5_hash
		if not over_write:
			try:
				f = open(file_name, 'r')
				data = json.load(f)
				try:
					hashes = [h['md5_hash'] for h in data if h['md5_hash']]
				except KeyError:
					hashes = []
					pass
				f.close()
				try:
					# Check for duplicate in json if duplicates are not allowed
					if spider.config['scrape']['allow_duplicates'] == 0:
						is_duplicate = self._is_duplicate(md5_hash, hashes)
					else:
						is_duplicate = False
					if not is_duplicate:
						data.append(entry)
				except:
					data = [entry]
			except Exception as e:
				data = [entry]
				#raise JSONReadError("Error in pipeline to append to current json: %s" % e)
				pass
		else:
			data = [entry]
		f = open(file_name, 'w+')
		f.write(json.dumps(data, indent=4, sort_keys=True, default=unicode))
		f.close()
		print("Wrote %s lines in %s" %(len(data), file_name) )
		return entry