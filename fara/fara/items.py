# -*- coding: utf-8 -*-
import scrapy

class FARAItem(scrapy.Item):
    country = scrapy.Field()
    state = scrapy.Field()
    reg_num = scrapy.Field()
    address = scrapy.Field()
    foreign_principal = scrapy.Field()
    date = scrapy.Field()
    registrant = scrapy.Field()
    exhibit_url = scrapy.Field()
    url = scrapy.Field()
    pass
