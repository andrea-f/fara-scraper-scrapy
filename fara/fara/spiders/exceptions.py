class NoMatchedKeys(Exception):
    """ [ERROR] Did not create a FARAItem! """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class PostValidationError(Exception):
    """
    [ERROR] Error in Post creation
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class FARAItemCreationError(Exception):
    """ [ERROR] Did not create a FARAItem! """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class NotAFullURL(Exception):
    """
    [ERROR] Url is relative and can't be used to perform request.
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass


class RequiredConfigKey(Exception):
    """
    [ERROR] MISSING REQUIRED VALUE!
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class CookiesInformationError(Exception):
    """
    [ERROR] In splitting and assigning cookies
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass    

class JSONReadError(Exception):
    """
    [ERROR] The requested resource does not exist.
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class WrongTypeError(Exception):
    """
    [ERROR] Wrong class instance type error.
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass    

class MaxNestedObjectsInInput(Exception):
    """
    [ERROR] Too many nested objects in dict
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass 

class MissingRequiredKey(Exception):
    """
    [ERROR] Object in pipeline missing required key, dropping item
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass 

class HTMLParseError(Exception):
    """
    [ERROR] HTML error in parsing scraped data
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass   