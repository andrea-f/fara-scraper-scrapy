# -*- coding: utf-8 -*-
import os
import sys
import json
import logging
from datetime import datetime
from types import NoneType
import scrapy
from fara.spiders.exceptions import (
    HTMLParseError,
    NoMatchedKeys,
    CookiesInformationError,
    RequiredConfigKey,
    JSONReadError,
    PostValidationError,
    WrongTypeError,
    MaxNestedObjectsInInput,
    NotAFullURL,
    FARAItemCreationError
)
from scrapy.selector import Selector
from scrapy.http import FormRequest, Request
from fara.items import FARAItem
from scrapy.utils.log import configure_logging
configure_logging(install_root_handler=False)
logging.basicConfig(
    filename='log.txt',
    filemode='a',
    format='%(levelname)s: %(message)s',
    level=logging.DEBUG
)


class FaraActivePrincipalsSpider(scrapy.Spider):

    name = "fara"
    custom_settings = {'REDIRECT_MAX_TIMES': 10}

    def get_required_keys(self):
        """ Returns required keys to check. """
        return [
            "api_url",
            "scrape.route_to_data",
            "scrape.object_mapping",
            "scrape.object_mapping.{}.rule",
            "scrape.object_mapping.{}.type",
            #"scrape.api_headers.cookies",
            "scrape.max_missing_mappings",
            "scrape.group_by",
            "scrape.sub_page_identifier",
            "scrape.root_url_separator",
            "root.url",
            "root.headers",
            "root.object_mapping",
            "api_post_data",
            "api_headers",
            "scrape.allow_duplicates"
        ]

    def __init__(self, file_name="input.json"):
        """
        Initialise class variables and create workers

        :param file_name:
            Where to read the input config from the file system, string.
        """
        self.input_config = file_name
        # Make sure input makes sense and has all required keys for program to run.
        self.required = self.get_required_keys()
        # Holds config information
        self.config = {}
        # It would be probably a good idea to move this out into its own class
        # self.object_mapping will be populated once config gets read in
        self.object_mapping = {}
        # Initialise array here for async requests
        self.sub_pages = []
        self.counter = 0
        self.hashes = []

    def check_required(self, input_config={}, required=[]):
        """
        Checks required values in configuration.

        :param required:
            Holds necessary keys needed in input config file(input.json) to execute scraper, list.
        :param input_config:
            Holds submitted input config, dict.
        Returns boolean with False if a required key is missing, otherwise True.

        """
        if len(required) == 0:
            required = self.required
        if len(input_config) == 0:
            input_config = self.config

        def __is_none(sub_obj):
            if isinstance(sub_obj, NoneType):
                return True
            return False

        def __check_required_keys_in_depth(required_input):
            """
            Checks that all required keys are present in input.

            :param r:
                key required in input config, string.
            Returns boolean False if required key is missing from input config dict.
            """
            # TODO: Refactor to be more concise
            # Split string by dot to check nested keys
            required_objects = required_input.split('.')
            exists = input_config.get(required_objects[0])
            if not exists:
                raise RequiredConfigKey("Missing top level input config key: %s" % required_input)
            if len(required_objects) == 2:
                leaf = input_config[required_objects[0]].get(required_objects[1])
                if isinstance(leaf, NoneType):
                    raise RequiredConfigKey("Missing sub level input config key: %s" % leaf)
            elif len(required_objects) > 2:
                if "{}" in required_objects:
                    for scrape_obj in input_config['scrape']['object_mapping'].keys():
                        #do string substitution
                        obj = '.'.join(required_objects).replace('{}', scrape_obj).split('.')[2]
                        # Could be optimised, it is repeating possibly without reason
                        # Assuming the input config goes only to the third level for verification.
                        sub_obj = input_config[required_objects[0]][required_objects[1]][obj].get(required_objects[3])
                        if __is_none(sub_obj):
                            return False
                else:
                    sub_obj = input_config[required_objects[0]][required_objects[1]][2].get(required_objects[3])
                    if not __check_none(sub_obj):
                        return False
            return True
        try:
            res = map(__check_required_keys_in_depth, required)
        except Exception as error:
            raise RequiredConfigKey("Missing when traversing input config and checking for required keys: %s" % error)

        def __nested_objects(input_config, counter=0):
            """
            Checks type of value from input key.

            :param input_config:
                Has input information to run scraping, dict.
            :param counter:
                Holds current dict depth, int.
            Returns max depth of dicts in input config.
            """
            if not isinstance(input_config, dict):
                return counter
            # @ stack suggestion
            return max(__nested_objects(v, counter+1) for k, v in input_config.iteritems())

        depth = __nested_objects(input_config)
        if depth > input_config["scrape"]["max_input_object_depth"]:
            raise MaxNestedObjectsInInput("Too many nested objects in input dict: %s, %s" % (depth, input_config["scrape"]["max_input_object_depth"]))
            return False
        if not isinstance(input_config['root']['object_mapping'], dict):
            raise RequiredConfigKey("Root object mapping in input is not a dict")
            return False
        if not isinstance(input_config['scrape']['max_missing_mappings'], int):
            raise RequiredConfigKey("Root max_missing_mappings in input is not an int!")
            return False
        if not input_config['scrape']['group_by'] in input_config['scrape']['object_mapping'].keys() and len(input_config['scrape']['group_by']) > 0:
            raise RequiredConfigKey("group_by key in input is not in scrape object_mapping")
            return False
        return True

    def read_config_file(self, file_name=""):
        """
        Reads json file with dbpedia query information

        :param filename:
            Location of JSON input file or a file like object like JSON in quotes
            with specified parameters to run scraper, string.
        Returns read JSON data.
        """
        if len(file_name) == 0:
            file_name = self.input_config
        if not os.path.isfile(file_name):
            file_name = "%s%s" % (os.getcwd(), file_name)
        try:
            with open(file_name, "r") as json_file:
                data = json.load(json_file)
                return data
        except:
            raise JSONReadError("[getConfigFile] Error in reading file: %s, quitting." % file_name)
            sys.exit(2)

    def _validate_headers(self, headers):
        """
        Validates headers object in input

        :param headers:
            Can contain Content-Type, User-Agent, Cookie information, dict.
        Returns dict with only valid headers found.
        """
        acceptable_headers = ['User-Agent', 'Content-Type']
        accepted_headers = {}
        try:
            for key, val in headers.iteritems():
                if (key in acceptable_headers and
                    not key in accepted_headers.keys() and
                        len(val) > 0 and isinstance(val, unicode) is True):
                    accepted_headers[key] = val
        except Exception as e:
            raise PostValidationError(e)
        return accepted_headers

    def __save_cookie_info(self, cookie):
        """
        Splits retrieved cookie values and assigns them to input config.
        Sets cookies in input config.

        :param cookie:
            Holds cookies retrieved from server response headers, string.
        """
        try:
            if isinstance(cookie, str):
                key = cookie.split('=')[0]
                val = cookie.split('=')[1]
                if len(key) > 0 and len(val) > 0:
                    self.config['api_headers']['cookies'][key] = val
        except Exception as e:
            raise CookiesInformationError("Error in splitting and assigning cookie %s" % e)

    def run_scrape_in_environment(self, response):
        """
        Set cookies so data can be requested.

        :param response:
            Contains HTTP response to request, class.
        """
        cookies = {}
        hxs = Selector(response=response)
        # Add any parameters in the body of the request to the input config

        def __get_root_body_payload(key):
            """
            Retrieves relevant keys set in the body response to the get function.

            :param key:
                Identifier to what information must be scraped from root page, string.
            """
            selector = self.config['root']['object_mapping'][key]['rule']
            try:
                values = hxs.xpath(selector).extract()
            except:
                raise HTMLParseError("Error in parsing HTML data with selector: %s" % selector)
            if len(values) > 0:
                # Assuming here there will be at least one variable
                self.config["api_post_data"][key] = values[0]
        map(__get_root_body_payload, self.config['root']['object_mapping'].keys())

        # Get any cookies from the response to be reused
        set_from_headers = response.headers.getlist('Set-Cookie')
        if len(set_from_headers) > 0:
            map(self.__save_cookie_info, set_from_headers)

        url = self.config['root']['url']
        if response.status and response.status != 200:
            # The clause here that if its a redirect url,
            # then call this function as callback again to yield
            # header with Location for redirection with cookies.
            try:
                if not response.headers['Location'] in url:
                    url = "%s%s" % (url, response.headers['Location'])
            except:
                pass
            if not "http" in url:
                raise NotAFullURL("%s is relative!" % url)
            yield Request(
                url,
                callback=self.run_scrape_in_environment,
                headers=self._validate_headers(self.config['root']['headers']),
                cookies=cookies,
                meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
            )
        else:
            try:
                url = self.config['api_url']
                if not "http" in url:
                    raise NotAFullURL("%s is relative!" % url)
                post_data = self.config['api_post_data']
            except KeyError as error:
                raise RequiredConfigKey("Missing api resource url api_url from config: %s  --- Current config: %s" % (error, self.config))
            headers = self._validate_headers(self.config['api_headers'])
            yield FormRequest(
                url,
                callback=self.parse,
                formdata=post_data,
                headers=headers,
                cookies=cookies,
                method="POST",
                meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
            )

    def start_requests(self):
        """
        First function to be called by scrapy!
        Built in scrapy function necessary in subclass of scrapy.Spider()
        """
        # Verify input configuration file
        if len(self.config) == 0:
            self.config = self.read_config_file()
        has_all_config_keys = self.check_required()
        if not has_all_config_keys:
            # Quit if we are missing a required config key,
            # if required keys are missing it compromises the integrity the script.
            raise RequiredConfigKey("Quit if we are missing a required config key!")
            sys.exit(2)
        yield FormRequest(
            self.config['root']['url'],
            callback=self.run_scrape_in_environment,
            formdata={},
            headers=self._validate_headers(self.config['root']['headers']),
            method="GET",
            meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
        )

    def __assign_to_row(self, extracted):
        """
        Assigns extracted to row.

        :param extracted:
            What was found in while scraping the page, list.
        Returns either string or list with extractd information
        """
        # Make sure that what we are adding has data
        if not "http" in extracted:
            retrieved = ' '.join(extracted)
        elif len(extracted) == 1:
            retrieved = extracted[0]
        else:
            retrieved = extracted
        return retrieved

    def parse(self, response,  row={}):
        """
        Parses repsonse from Scrapy scraping.

        :param response:
            Contains HTTP response to request, class.
        :param entries:
            Will hold scraped data, dict.
        :param row:
            Initializes empty var, will hold single extracted line, dict.
        Returns function call to process subpages
        """
        if len(self.config['scrape']['group_by']) > 0:
            entries = {}
        else:
            entries = []
        hxs = Selector(response=response)
        selector = self.config['scrape']['route_to_data']
        try:
            data_container = hxs.xpath(selector)
        except:
            raise HTMLParseError("Error in parsing HTML data with selector: %s" % selector)
        # Make sure to not trip over on last slash in URL
        if not self.config['root']['url'].endswith('/'):
            self.config['root']['url'] = self.config['root']['url']+"/"
        for item in data_container:
            for key, val in self.config['scrape']['object_mapping'].iteritems():
                try:
                    extracted = item.xpath(val['rule']).extract()
                except:
                    raise HTMLParseError("Error in parsing HTML data with selector: %s" % val['rule'])
                if isinstance(entries, dict):
                    if self.config['scrape']['group_by'] in key:
                        if len(extracted) == 1:
                            current_group = extracted[0]
                            entries[current_group] = []
                            # Reset row to start next capture
                            row = {}
                    else:
                        if len(extracted) > 0:
                            row[key] = self.__assign_to_row(extracted)
                else:
                    if len(extracted) > 0:
                        row[key] = self.__assign_to_row(extracted)

            # Check that difference between the current populated object (row)
            # matches as close as possible to what we need
            diff = self._asses_diff(row)
            # Does the current row contain enough data to be saved?
            # Assuming that if there are more than 'max_missing_mappings'
            # its not worth to save object
            if diff <= self.config['scrape']['max_missing_mappings']:
                if isinstance(entries, dict):
                    row[self.config['scrape']['group_by']] = current_group
                    #
                    # Populates retrieved items here
                    #
                    entries[current_group].append(row)
                    # Reset row to start next capture
                else:
                    entries.append(row)
                row = {}
        # Update retrieved objects with possibly links
        new_requests = self._scrape_links(entries)
        return self.scrape_further_data(new_requests)

    def scrape_further_data(self, new_requests):
        """
        Performs the yield which contains a request

        :param new_requests:
            Contains entries to be tranformed in Request class objects, list.
        """
        self.sub_pages = [
            Request(
                i['url'],
                callback=self.look_for_missing_data,
                headers=self._validate_headers(self.config['root']['headers']),
                method="GET",
                meta={"item": i}
            ) for i in new_requests
        ]
        if len(self.sub_pages) > 0:
            yield self.sub_pages[0]

    def _scrape_links(self, entries):
        """
        Uses input config key to identify links in the retrieved fields

        :param entries:
            Holds scraped objects from HTML by group_by key as set in input config, dict.
        Returns list with updated objects.
        """
        new_requests = []
        # Get subpages
        try:
            sub_page_identifier = self.config['scrape']['sub_page_identifier']
        except:
            sub_page_identifier = ""

        def __convert_url_to_full(item):
            """
            Adds root url if current url is relative.

            :param item:
                Has information defined in scrape object mapping, dict.
            """
            # For each entry in item, is an entry with country etc
            try:
                val = item[sub_page_identifier]
                if not "http" in val:
                    url = "%s%s" % (self.config['root']['url'].split(self.config['scrape']['root_url_separator'])[0], val)
                else:
                    url = val
                item[sub_page_identifier] = url
            except Exception as e:
                raise NotAFullURL("Error in setting full url: %s" % e)
                pass
                # Will be populated with all items
            new_requests.append(item)

        for k, v in entries.iteritems():
            # Check in sub dictionary for links to explore
            # k is a country, like vietnam
            # v are the entries associated with that country
            map(__convert_url_to_full, v)

        return new_requests

    def run_sub_pages(self, counter=0, item={}, sub_pages=[], stop=False):
        """
        Process sub pages requests, save item if its complete.

        :param counter:
            Holds order of needle in sub_pages stack, int.
        :param sub_pages:
            Holds scarpy Request instance object, class.
        """
        # Make sure the list contains a class of type Request
        if len(sub_pages) == 0:
            sub_pages = self.sub_pages
        if counter <= len(sub_pages):
            # To account for last element
            if counter == len(sub_pages):
                diff = -1
                stop = True
            else:
                diff = 0
            if isinstance(sub_pages[counter+diff], scrapy.http.request.Request):
                try:
                    ##### Might not be really needed?
                    diff = self._asses_diff(item)
                    if diff > 0:
                        raise FARAItemCreationError(" There is at least one difference between the fully retrieved object and the necessary data ")
                        pass
                    entry = FARAItem(**item)
                    # Send to next step in pipeline
                    yield entry
                except Exception as e:
                    raise FARAItemCreationError("Didn't create an item because :%s" % e)
                if not stop:
                    yield self.sub_pages[counter]
                else:
                    # Clean up
                    self.sub_pages = []
            else:
                raise WrongTypeError("Expected self.sub_pages[counter] to be an instance of Request class instead got: %s" % type(self.sub_pages[counter]))

    def _format_retrieved_object(self, entry):
        """
        Format already populated key, uniform keys.

        :param entry:
            Contains already matched data from scraped HTML, dict.
        Returns dict with .title() applied to values.

        """
        for key in self.config['scrape']['object_mapping'].keys():
            try:
                if isinstance(entry[key], unicode) and not "http" in entry[key]:
                    entry[key] = entry[key].title()
                    try:
                        entry[key] = entry[key].replace(u'\u00a0\u00a0', ' ').strip()
                    except:
                        pass
                    try:
                        # TODO: improve checking of key if only one item don't convert to string
                        # Otherwise will drop
                        if len(entry[key]) and "int" in self.config['scrape']['object_mapping'][key]['type']:
                            entry[key] = int(entry[key])
                    except: pass
                    try:
                        entry[key] = datetime.strptime(entry[key], '%m/%d/%Y')
                    except:
                        pass
            except KeyError:
                entry[key] = None
        return entry

    def look_for_missing_data(self, response):
        """
        Tries to navigate tree links to find relevant information from sub pages as defined in input config.
        :param response:
            Contains HTTP response to request from Scrapy middleware, class.
        Returns function to save item and execute next query
        """
        hxs = Selector(response=response)
        # For inner method, remove dict and copy
        i = dict(response.meta['item'].copy())
        # Set missing keys to None
        # Try to populate missing data
        entry = self._format_retrieved_object(i)
        matching_keys = 0
        try:
            must_have_matching = self.config['scrape']['must_have_matching']
        except KeyError:
            must_have_matching = False
            pass
        for key in self.config['scrape']['object_mapping'].keys():
            selector = self.config['scrape']['object_mapping'][key]['rule']
            try:
                data = hxs.xpath(selector).extract()
            except:
                raise HTMLParseError("Error in parsing HTML data with selector: %s" % selector)
            if entry[key] is None and len(data) > 0:
                entry[key] = data
        ######## Assuming it is, but might not be really needed
            else:
                if (must_have_matching and
                    not isinstance(entry[key], list) and
                        unicode(entry[key]).lower() in [d.lower() for d in data]):
                            matching_keys += 1
        if must_have_matching and matching_keys == 0:
            raise NoMatchedKeys("No keys were matched ! ")
        self.counter += 1
        # Expose updated entry
        # Not really needed because the dict in response.meta['item']
        # is automatically updated within the sub_pages list, so it could be accessed by:
        # item = self.sub_pages[counter-1].meta["item"] in run_sub_pages
        return self.run_sub_pages(counter=self.counter, item=entry)

    def _asses_diff(self, entry):
        """
        Check that we have found the necessary data with variable range of accuracy

        :param entry:
            Retrieved object from scraper to compare with required object mappings, dict.
        Returns int with distance from how complete the object is vs the required mappings.
        """
        keys_in_object = len(entry.keys())
        keys_in_reference = len(self.config['scrape']['object_mapping'])
        return abs(keys_in_object-keys_in_reference)
