# README #

FARA scraper with Scrapy

### What is this repository for? ###

* Scrape all Active Foreign National information from FARA website with Scrapy in Python 2.7
* Version: 0.1

### How do I get set up? ###

Set the scraper up using the default input config defined in `input.json`:

1. Create a virtual env in a directory: `me@somedir?> virtualenv scraping_with_scrapy`
2. Activate env: `me@somedir?> source scraping_with_scrapy/bin/activate`
3. Install requirements with PIP `me@somedir?> pip install -r req.txt`
4. Unzip package or, with read access, clone repo: `git clone git@bitbucket.org:andrea-f/fara-scraper-scrapy.git`
5. `me@somedir?> cd fara-scraper-scrapy/fara/fara/spiders`
6. Run Scrapy project: `me@somedir?> scrapy crawl fara`
7. Output will be in current directory, full output for requirements in full_output.json

 

### The logic behind it

Scrapy is python toolkit to get information from websites programmatically and turn it into structured data.
It bundles together all components of any scraper out of the box. In order to fullfil the requirements, the project
was started as a Scrapy project, then customised to download all the data from https://www.fara.gov/quick-search.html​ (Click "Active Principals").
For modularisation, testing and readability an input config strategy has been implemented.
After cloning/unpacking the repo full input configuration options: `fara-scraper-scrapy/fara/fara/spiders`. 

#### Assumptions ###

In order to provide a complete solution to the challenge, some assumptions were made, these are also reflected in input.json:

* In `fara_active_principals.FaraActivePrincipalsSpider.parse`: assuming that if there are more than 'max_missing_mappings', when compared to number of keys in scrape object mappings, from a retrieved HTML resource to populate an object, the object can be saved. Otherwise keep traversing the DOM.

* In `fara_active_principals.FaraActivePrincipalsSpider.parse__checkRequired`: assuming the input config goes only to the second level, only one level of nested dictionaries is allowed in input.json. This is for input verification purposes. Otherwise throws a MaxNestedObjectsInInput error. The same assumption is made in `fara_active_principals.FaraActivePrincipalsSpider.run_scrape_in_environment`.

* Assuming that there can be duplicates in the json, across different cold runs of the program, because: simulation test - not using a database, would be expensive to load the JSON file and check for duplicates without an extra hash key in the data requirements. Duplicate check though is enforced in current instance.

* Assuming missing required values in input config stops the algorithm

* Did not use Item Contracts to test because wanted a more general, site agnostic solution. Used list(generator) to test input/output values.

* Assuming state key is fine with upper initial, lower second letter, same for address.

#### Where is the data coming from

Two data sources are used to collect all the necessary information, these were provided by:

* Manually opening https://www.fara.gov/quick-search.html​ (Click "Active Principals")

* By changing the search box options -> Actions, selecting the view to be of all records and using Chrome Developer Tools to inspect the site network traffic, it can be deduced that the FARA public database output is listening on: https://efile.fara.gov/pls/apex/wwv_flow.show

* By recreating the request conditions with Scrapy, it is possible to optain all the records without the hassle of pagination.

* After having identified, via the input config file, each specific element from that initial request, the links in the page to the detail of an association are requested, via a GET request. To order an asynchronous stream of responses, due to Twisted engine being used, a class counter and stack is used. This ensures that the responses are associated to the right objects.


#### Testing ####

Tests are written in unittest and run with Nose. To run: `me@somedir?> nosetests FaraScrapeTest.py`
For coverage report (files HTML will be in tests/cover/index.html): `nosetests FaraScrapeTest.py --with-coverage --cover-html`
Current coverage: `fara/spiders/fara_active_principals.py` 95%+

#### Results ###

Algorithm runs in:
- 4 minutes and 10 seconds for 508 entries, ~ 2 entries per second processed.
- delay time for network requests